package ua.kpi.tef.view;

import java.util.Locale;
import java.util.ResourceBundle;

public interface ViewConstants {

    String INPUT_COMMENT = "comment";
    String INPUT_REGEX = "regex";
    String INPUT_REQUIRED = "required";
    String INPUT_ADVICE = "advice";
    String REQUIRED_CORRECT_RESPONSE = "yes";
    String INPUT_ORDER = "ordinal";
    String APP_LANGUAGE = "application.language";

    ResourceBundle COMMON_BUNDLE = ResourceBundle.getBundle("common");
    ResourceBundle INTERNATIONAL_BUNDLE = ResourceBundle.getBundle(
            "international",
            Locale.forLanguageTag(COMMON_BUNDLE.getString(APP_LANGUAGE)));

    String FULL_ADDRESS_FORMAT = "%s %s, apt. %s, %s %s";
    String FULL_SHORT_FORMAT = "%s %c.";
}
