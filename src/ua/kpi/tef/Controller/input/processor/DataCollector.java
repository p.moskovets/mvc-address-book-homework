package ua.kpi.tef.controller.input.processor;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static ua.kpi.tef.view.ViewConstants.*;

public class DataCollector {

    public static final char PATH_DELIMITER = '.';
    public static final String REGEX_DELIMITER = "|";
    public static final String ADVICE_DELIMITER = ", ";

    private static final Map<Class<?>, Function<String, ?>> CONVERTERS = Map.of(
            String.class, String::valueOf,
            Integer.class, Integer::valueOf,
            Long.class, Long::valueOf,
            Double.class, Double::valueOf,
            Float.class, Float::valueOf,
            Character.class, (s) -> s.charAt(0),
            Short.class, Short::valueOf,
            Byte.class, Byte::valueOf,
            LocalDateTime.class, LocalDateTime::parse);


    protected Map<Class<?>, Function<String, ?>> enumConverters() {
        return Map.of();
    }

    private Type getType(Class<?> clazz) {
        Function<String, ?> func = CONVERTERS.get(clazz);
        if (func != null)
            return Type.PRIMITIVE;
        func = enumConverters().get(clazz);
        if (func != null)
            return Type.ENUM;
        return Type.USER;
    }

    private static String getCommon(String fieldPath, String parameter) {
        return COMMON_BUNDLE.getString(fieldPath + PATH_DELIMITER + parameter);
    }

    private static boolean isRequired(String fieldPath) {
        return Boolean.parseBoolean(getCommon(fieldPath, INPUT_REQUIRED));
    }

    private static int getOrdinal(String fieldPath) {
        return Integer.parseInt(getCommon(fieldPath, INPUT_ORDER));
    }

    private static String getEnumString(Class<Enum<?>> e, String delimiter) {
        return Arrays.stream(e.getEnumConstants()).map(Enum::toString).collect(Collectors.joining(delimiter));
    }

    private static String getInternational(String fieldPath, String parameter) {
        return INTERNATIONAL_BUNDLE.getString(fieldPath + PATH_DELIMITER + parameter);
    }

    private static String getInputValue(StringInputFunction inputFunction, String fieldPath) {
        return inputFunction.input(getInternational(fieldPath, INPUT_COMMENT),
                getInternational(fieldPath, INPUT_REGEX),
                getInternational(fieldPath, INPUT_ADVICE));
    }

    private static String getEnumInputValue(StringInputFunction inputFunction, String fieldPath, Class<Enum<?>> e) {
        return inputFunction.input(getInternational(fieldPath, INPUT_COMMENT),
                getEnumString(e, REGEX_DELIMITER),
                getInternational(fieldPath, INPUT_ADVICE) + getEnumString(e, ADVICE_DELIMITER));
    }

    @SuppressWarnings("unchecked")
    private void processField(StringInputFunction function, Consumer<String> printer, Field field, String fieldPath) {
        Class<?> fieldType = field.getType();
        Type type = getType(fieldType);
        try {
            if (type == Type.PRIMITIVE) {
                field.set(this, CONVERTERS.get(fieldType).apply(getInputValue(function, fieldPath)));
            } else if (type == Type.ENUM) {
                field.set(this, enumConverters().get(fieldType).apply(getEnumInputValue(function,
                        fieldPath,
                        (Class<Enum<?>>) fieldType)));
            } else {
                field.set(this, fieldType.getDeclaredConstructor().newInstance());
                ((DataCollector) field.get(this)).inputData(function, printer);
            }
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }

    public void inputData(StringInputFunction function, Consumer<String> printer) {
        String className = getClass().getName() + PATH_DELIMITER;
        var fields = Arrays.stream(getClass().getDeclaredFields())
                .sorted(Comparator.comparing((item -> getOrdinal(className + item.getName()))))
                .collect(Collectors.toList());
        for (Field field : fields) {
            field.setAccessible(true);
            String fieldPath = className + field.getName();
            if (isRequired(fieldPath)) {
                processField(function, printer, field, fieldPath);
            } else {
                if (getInternational(INPUT_REQUIRED, REQUIRED_CORRECT_RESPONSE)
                        .equals(getInputValue(function, INPUT_REQUIRED))) {
                    processField(function, printer, field, fieldPath);
                }
            }
        }

    }
}
