package ua.kpi.tef.controller.input.processor;

public enum Type {
    PRIMITIVE,
    ENUM,
    USER;
}
