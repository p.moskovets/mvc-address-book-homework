package ua.kpi.tef.controller.input.processor;

@FunctionalInterface
public interface StringInputFunction {
    String input(String comment, String pattern, String advice);
}
