package ua.kpi.tef.controller.input;

import ua.kpi.tef.model.data.PersonNameModel;
import ua.kpi.tef.controller.input.processor.DataCollector;

public class InputPersonName extends DataCollector {
    private String lastName;
    private String firstName;
    private String middleName;

    public PersonNameModel getPersonName() {
        return new PersonNameModel(lastName, firstName, middleName);
    }
}
