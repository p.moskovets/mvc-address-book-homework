package ua.kpi.tef.controller.input;

import ua.kpi.tef.model.data.AddressModel;
import ua.kpi.tef.controller.input.processor.DataCollector;

public class InputAddress extends DataCollector {

    private String zipCode;
    private String city;
    private String street;
    private String houseNumber;
    private Integer flatNumber;

    public AddressModel getAddress() {
        return new AddressModel(zipCode, city, street, houseNumber, flatNumber);
    }
}
