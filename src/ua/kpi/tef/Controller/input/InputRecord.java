package ua.kpi.tef.controller.input;


import ua.kpi.tef.model.data.AddressModel;
import ua.kpi.tef.model.data.Group;
import ua.kpi.tef.model.data.PersonNameModel;
import ua.kpi.tef.model.data.RecordModel;
import ua.kpi.tef.controller.input.processor.DataCollector;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.function.Function;

public class InputRecord extends DataCollector {

    private InputPersonName fullName;
    private String nickName;
    private String comment;
    private Group group;
    private String homePhoneNumber;
    private String cellPhoneNumber;
    private String additionalCellPhoneNumber;
    private String email;
    private String skype;
    private InputAddress address;
    private LocalDateTime createTimeStamp;
    private LocalDateTime lastModifiedTimeStamp;

    @Override
    protected Map<Class<?>, Function<String, ?>> enumConverters() {
        return Map.of(Group.class, Group::ofString);
    }

    public RecordModel getRecord() {
        LocalDateTime dateTime = LocalDateTime.now();
        return new RecordModel(getPersonName(),
                nickName,
                comment,
                group,
                homePhoneNumber,
                cellPhoneNumber,
                additionalCellPhoneNumber,
                email,
                skype,
                getAddress(),
                dateTime,
                dateTime);
    }

    private PersonNameModel getPersonName() {
        return fullName == null ? null : fullName.getPersonName();
    }

    private AddressModel getAddress() {
        return address == null ? null : address.getAddress();
    }
}
