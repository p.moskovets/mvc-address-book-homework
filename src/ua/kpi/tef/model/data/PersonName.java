package ua.kpi.tef.model.data;

public interface PersonName {
    int INDEX_OF_THE_FIRST_CHAR = 0;

    String getLastName();
    String getFirstName();
    String getMiddleName();


    default String getFullShortName(String fullShortNameFormat) {
        return String.format(fullShortNameFormat,
                getLastName(),
                getFirstName().charAt(INDEX_OF_THE_FIRST_CHAR));
    }

}
