package ua.kpi.tef.model.data;

public interface Address {
    String getZipCode();
    String getCity();
    String getStreet();
    String getHouseNumber();
    Integer getFlatNumber();

    default String getFullAddress(String addressFormat) {
        return String.format(addressFormat, getStreet(),
                getHouseNumber(),
                getFlatNumber(),
                getZipCode(),
                getCity());
    }
}
