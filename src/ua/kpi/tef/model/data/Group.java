package ua.kpi.tef.model.data;

public enum Group {
    FIRST("1a"),
    SECOND("2b"),
    THIRD("3c");

    private final String groupName;

    Group(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return groupName;
    }

    public static Group ofString(String s) {
        for (Group g : Group.values()){
            if (g.toString().equals(s))
                return g;
        }
        return null;
    }
}
