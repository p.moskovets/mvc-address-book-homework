package ua.kpi.tef.model.data;

import java.time.LocalDateTime;

public interface Record {
    PersonName getFullName();
    String getNickName();
    String getComment();
    Group getGroup();
    String getHomePhoneNumber();
    String getCellPhoneNumber();
    String getAdditionalCellPhoneNumber();
    String getEmail();
    String getSkype();
    Address getAddress();
    LocalDateTime getCreateTimeStamp();
    LocalDateTime getLastModifiedTimeStamp();
}
