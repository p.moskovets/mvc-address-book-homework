package ua.kpi.tef.model.data;

import java.time.*;

public class RecordModel implements Record {
    private PersonNameModel fullName;
    private String nickName;
    private String comment;
    private Group group;
    private String homePhoneNumber;
    private String cellPhoneNumber;
    private String additionalCellPhoneNumber;
    private String email;
    private String skype;
    private AddressModel addressModel;
    private LocalDateTime createTimeStamp;
    private LocalDateTime lastModifiedTimeStamp;

    public RecordModel(PersonNameModel fullName,
                       String nickName,
                       String comment,
                       Group group,
                       String homePhoneNumber,
                       String cellPhoneNumber,
                       String additionalCellPhoneNumber,
                       String email,
                       String skype,
                       AddressModel addressModel,
                       LocalDateTime createTimeStamp,
                       LocalDateTime lastModifiedTimeStamp) {
        this.fullName = fullName;
        this.nickName = nickName;
        this.comment = comment;
        this.group = group;
        this.homePhoneNumber = homePhoneNumber;
        this.cellPhoneNumber = cellPhoneNumber;
        this.additionalCellPhoneNumber = additionalCellPhoneNumber;
        this.email = email;
        this.skype = skype;
        this.addressModel = addressModel;
        this.createTimeStamp = createTimeStamp;
        this.lastModifiedTimeStamp = lastModifiedTimeStamp;
    }


    @Override
    public PersonNameModel getFullName() {
        return fullName;
    }

    public void setFullName(PersonNameModel fullName) {
        this.fullName = fullName;
    }

    @Override
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Override
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public String getHomePhoneNumber() {
        return homePhoneNumber;
    }

    public void setHomePhoneNumber(String homePhoneNumber) {
        this.homePhoneNumber = homePhoneNumber;
    }

    @Override
    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    @Override
    public String getAdditionalCellPhoneNumber() {
        return additionalCellPhoneNumber;
    }

    public void setAdditionalCellPhoneNumber(String additionalCellPhoneNumber) {
        this.additionalCellPhoneNumber = additionalCellPhoneNumber;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    @Override
    public AddressModel getAddress() {
        return addressModel;
    }

    public void setAddress(AddressModel addressModel) {
        this.addressModel = addressModel;
    }

    @Override
    public LocalDateTime getCreateTimeStamp() {
        return createTimeStamp;
    }

    public void setCreateTimeStamp(LocalDateTime createTimeStamp) {
        this.createTimeStamp = createTimeStamp;
    }

    @Override
    public LocalDateTime getLastModifiedTimeStamp() {
        return lastModifiedTimeStamp;
    }

    public void setLastModifiedTimeStamp(LocalDateTime lastModifiedTimeStamp) {
        this.lastModifiedTimeStamp = lastModifiedTimeStamp;
    }
}
